#include <stdio.h>
#include "spi_software.h"

/**********delay****************/
void spi_device_demo_delay_us(unsigned char us)
{
}

// 配置CS 方向
/**********CS****************/
void spi_device_demo_cfg_ss_dir(unsigned char dir)
{
    //if(dir == IOPORT_DIR_OUTPUT)
}
void spi_device_demo_set_ss_pin(void)
{
}
void spi_device_demo_clr_ss_pin(void)
{
}

/**********CLK****************/
void spi_device_demo_cfg_clk_dir(unsigned char dir)
{
    //if(dir == IOPORT_DIR_OUTPUT)
}
void spi_device_demo_set_clk_pin(void)
{
}
void spi_device_demo_clr_clk_pin(void)
{
}

/**********SDO****************/
void spi_device_demo_cfg_sdo_dir(unsigned char dir)
{
    //if(dir == IOPORT_DIR_OUTPUT)
}
void spi_device_demo_set_sdo_pin(void)
{
}
void spi_device_demo_clr_sdo_pin(void)
{
}

/**********SDI****************/
void spi_device_demo_cfg_sdi_dir(unsigned char dir)
{
    //if(dir == IOPORT_DIR_INPUT)
}
unsigned char spi_device_demo_get_sdi_pin(void)
{
}
void spi_device_demo_clr_sdi_pin(void)
{
}

/**********INFO****************/
struct spi_device_info spi_device_demo =
{
    .cmm_mode = Mode_1,
    .do_delay_us = spi_device_demo_delay_us,
    // CS
    .cfg_ss_dir = spi_device_demo_cfg_ss_dir,
    .set_ss_pin = spi_device_demo_set_ss_pin,
    .clr_ss_pin = spi_device_demo_clr_ss_pin,
    // CLK
    .cfg_clk_dir = spi_device_demo_cfg_clk_dir,
    .set_clk_pin = spi_device_demo_set_clk_pin,
    .clr_clk_pin = spi_device_demo_clr_clk_pin,
    // SDO/MOSI
    .cfg_sdo_dir = spi_device_demo_cfg_sdo_dir,
    .set_sdo_pin = spi_device_demo_set_sdo_pin,
    .clr_sdo_pin = spi_device_demo_clr_sdo_pin,
    // SDI/MISO
    .cfg_sdi_dir = spi_device_demo_cfg_sdi_dir,
    .get_sdi_pin = spi_device_demo_get_sdi_pin,
};

int main(int argc, char * argv[])
{
    SPISimulateInit(&spi_device_demo);
    return 0;
}
