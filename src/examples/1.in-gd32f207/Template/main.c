/*!
  \file    main.c
  \brief   led spark with systick, USART print and key example

  \version 2023-06-30, V2.5.0, firmware for GD32F20x
  */

/*
   Copyright (c) 2023, GigaDevice Semiconductor Inc.

   Redistribution and use in source and binary forms, with or without modification,
   are permitted provided that the following conditions are met:

   1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
   2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.
   3. Neither the name of the copyright holder nor the names of its contributors
   may be used to endorse or promote products derived from this software without
   specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
   WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
   IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
   INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
   NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
   WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
   OF SUCH DAMAGE.
   */

#include "main.h"
#include "systick.h"
#include "gd32f20x_eval.h"
#include "usart.h"

#include <stdio.h>

#if 1
//////////////////////////////// gpio SPI /////////////////////////////
#include "gpio-spi.h"
//1262 delay
//#define HAL_Delay_nMS(n)            delay_1ms(n)

//1262 CS
#define P03_CS_L                    spi_device_demo_clr_nss_pin()
#define P03_CS_H                    spi_device_demo_set_nss_pin()

//1262 spi TX/RX
#define Spi_SendData(u8Data)        SPISimulate_WriteByte(&spi_device_demo, u8Data)
#define Spi_ReceiveData()           SPISimulate_ReadByte(&spi_device_demo)

//1262 reset
#define reset_low()                 gpio_bit_reset(GPIOE, GPIO_PIN_4)
#define reset_hig()                 gpio_bit_set(GPIOE, GPIO_PIN_4)

//1262 SW ctrl
#define sw_ctrl_on()                gpio_bit_set(GPIOE, GPIO_PIN_3)
#define sw_ctrl_off()               gpio_bit_reset(GPIOE, GPIO_PIN_3)

//1262 get busy status
#define get_busy_status()           gpio_input_bit_get(GPIOE, GPIO_PIN_5)

//1262 dio status
#define get_dio_status()            gpio_input_bit_get(GPIOE, GPIO_PIN_2)

#endif

/*
   void bsp_spi_io_init(void)
   {
   rcu_periph_clock_enable(RCU_GPIOB);
   rcu_periph_clock_enable(RCU_GPIOC);
   gpio_init(GPIOC, GPIO_MODE_IN_FLOATING, GPIO_OSPEED_50MHZ, GPIO_PIN_2);//MISO/PC2
   gpio_init(GPIOC, GPIO_MODE_OUT_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_3);//MOSI/PC3
   gpio_init(GPIOB, GPIO_MODE_OUT_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_10);//SCK/PB10
   gpio_init(GPIOB, GPIO_MODE_OUT_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_9);//NSS/PB9
   }
   */

/**********delay****************/
void spi_device_demo_delay_us(unsigned char us)
{
    delay_1us(us*10);
}

/**********CS****************/
void spi_device_demo_cfg_nss_dir(unsigned char dir)
{
    rcu_periph_clock_enable(RCU_GPIOB);
    gpio_init(GPIOB, GPIO_MODE_OUT_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_9);//NSS/PB9
}
void spi_device_demo_set_nss_pin(void)
{
    gpio_bit_set(GPIOB, GPIO_PIN_9);
}
void spi_device_demo_clr_nss_pin(void)
{
    gpio_bit_reset(GPIOB, GPIO_PIN_9);
}

/**********CLK****************/
void spi_device_demo_cfg_clk_dir(unsigned char dir)
{
    //if(dir == IOPORT_DIR_OUTPUT)
    rcu_periph_clock_enable(RCU_GPIOB);
    gpio_init(GPIOB, GPIO_MODE_OUT_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_10);//SCK/PB109
}
void spi_device_demo_set_clk_pin(void)
{
    gpio_bit_set(GPIOB, GPIO_PIN_10);
}
void spi_device_demo_clr_clk_pin(void)
{
    gpio_bit_reset(GPIOB, GPIO_PIN_10);
}

/**********SDO****************/
void spi_device_demo_cfg_sdo_dir(unsigned char dir)
{
    //if(dir == IOPORT_DIR_OUTPUT)
    rcu_periph_clock_enable(RCU_GPIOC);
    gpio_init(GPIOC, GPIO_MODE_OUT_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_3);//MOSI/PC3

}
void spi_device_demo_set_sdo_pin(void)
{
    gpio_bit_set(GPIOC, GPIO_PIN_3);
}
void spi_device_demo_clr_sdo_pin(void)
{
    gpio_bit_reset(GPIOC, GPIO_PIN_3);
}

/**********SDI****************/
void spi_device_demo_cfg_sdi_dir(unsigned char dir)
{
    //if(dir == IOPORT_DIR_INPUT)
    rcu_periph_clock_enable(RCU_GPIOC);
    gpio_init(GPIOC, GPIO_MODE_IN_FLOATING, GPIO_OSPEED_50MHZ, GPIO_PIN_2);//MISO/PC2
}
unsigned char spi_device_demo_get_sdi_pin(void)
{
    return gpio_input_bit_get(GPIOC, GPIO_PIN_2);
}

/**********INFO****************/
struct spi_device_info spi_device_demo =
{
    .cmm_mode = Mode_1,
    .do_delay_us = spi_device_demo_delay_us,
    // CS
    .cfg_nss_dir = spi_device_demo_cfg_nss_dir,
    .set_nss_pin = spi_device_demo_set_nss_pin,
    .clr_nss_pin = spi_device_demo_clr_nss_pin,
    // CLK
    .cfg_clk_dir = spi_device_demo_cfg_clk_dir,
    .set_clk_pin = spi_device_demo_set_clk_pin,
    .clr_clk_pin = spi_device_demo_clr_clk_pin,
    // SDO/MOSI
    .cfg_sdo_dir = spi_device_demo_cfg_sdo_dir,
    .set_sdo_pin = spi_device_demo_set_sdo_pin,
    .clr_sdo_pin = spi_device_demo_clr_sdo_pin,
    // SDI/MISO
    .cfg_sdi_dir = spi_device_demo_cfg_sdi_dir,
    .get_sdi_pin = spi_device_demo_get_sdi_pin,
};

/**********APP RW****************/
#define RADIO_READ_REGISTER  0xff
#define RADIO_WRITE_REGISTER 0x00
void SX126xReadRegisters( uint16_t address, uint8_t *buffer, uint16_t size )
{
    //SX126xCheckDeviceReady( );

    P03_CS_L;

    Spi_SendData(RADIO_READ_REGISTER );
    Spi_SendData(( address & 0xFF00 ) >> 8 );
    Spi_SendData( address & 0x00FF );
    Spi_SendData( 0 );
    for( uint16_t i = 0; i < size; i++ )
    {
        buffer[i] = Spi_ReceiveData();
    }

    P03_CS_H;

    //SX126xWaitOnBusy( );
}

void SX126xWriteRegisters( uint16_t address, uint8_t *buffer, uint16_t size )
{
    //SX126xCheckDeviceReady( );

    //P03_CS_L;

    Spi_SendData(RADIO_WRITE_REGISTER );
    Spi_SendData(( address & 0xFF00 ) >> 8 );
    Spi_SendData( address & 0x00FF );

    for( uint16_t i = 0; i < size; i++ )
    {
        Spi_SendData(buffer[i] );
    }


    //P03_CS_H;

    //SX126xWaitOnBusy( );
}

int main(void)
{
    unsigned char data1[1] = {0x57};

    /* configure systick */
    systick_config();

    usart1_init();

    SPISimulateInit(&spi_device_demo);

    SX126xReadRegisters( 0x06BC, data1, 1 );
    SX126xWriteRegisters(0x06C0, data1, 1 );

    while(1) { };
}

/* retarget the C library printf function to the USART */
#if 0
int fputc(int ch, FILE *f)
{
    usart_data_transmit(EVAL_COM1, (uint8_t)ch);
    while(RESET == usart_flag_get(EVAL_COM1, USART_FLAG_TBE));

    return ch;
}
#endif
