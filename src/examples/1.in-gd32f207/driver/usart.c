#include "usart.h"
#include <string.h>

uint8_t rxbuffer;


/*!
    \brief      this function handles USART0 exception
    \param[in]  none
    \param[out] none
    \retval     none
*/
void USART1_IRQHandler(void)
{
    if(RESET != usart_interrupt_flag_get(USART1, USART_INT_FLAG_RBNE))
    {
        /* receive data */
        rxbuffer = usart_data_receive(USART1);
        if(rxbuffer == 0x12)
        {
            usart_data_transmit(USART1, 0x77);
        }
    }

//    if(RESET != usart_interrupt_flag_get(USART1, USART_INT_FLAG_IDLE))
//    {
//        /* receive data */
//        usart_data_transmit(USART1, 0x88);
//    }

    //清除中断标志位
    usart_flag_clear(USART1, USART_FLAG_RBNE);
//    usart_flag_clear(USART1, USART_FLAG_IDLE);
}


/*!
    \brief      configure usart1
    \param[in]
      \arg
    \param[out] none
    \retval     none
*/
void usart1_init(void)
{
    /* USART interrupt configuration */
    nvic_irq_enable(USART1_IRQn, 0, 0);

    /* enable GPIO clock */
    rcu_periph_clock_enable(RCU_GPIOD);
    rcu_periph_clock_enable(RCU_GPIOA);
    rcu_periph_clock_enable(RCU_AF);

    /* enable USART clock */
    rcu_periph_clock_enable(RCU_USART1);

    /* connect port to USARTx_Tx */
    gpio_init(GPIOD, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_5);

    /* connect port to USARTx_Rx */
    gpio_init(GPIOD, GPIO_MODE_IN_FLOATING, GPIO_OSPEED_50MHZ, GPIO_PIN_6);

    gpio_pin_remap_config(GPIO_USART1_REMAP, ENABLE);

    /* USART configure */
    usart_deinit(USART1);
    usart_baudrate_set(USART1, 115200U);
    usart_word_length_set(USART1, USART_WL_8BIT);
    usart_stop_bit_set(USART1, USART_STB_1BIT);
    usart_parity_config(USART1, USART_PM_NONE);
    usart_hardware_flow_rts_config(USART1, USART_RTS_DISABLE);
    usart_hardware_flow_cts_config(USART1, USART_CTS_DISABLE);
    usart_receive_config(USART1, USART_RECEIVE_ENABLE);
    usart_transmit_config(USART1, USART_TRANSMIT_ENABLE);

    usart_enable(USART1);

    /* enable idle interrupt */
    usart_interrupt_enable(USART1, USART_INT_IDLE);
    usart_interrupt_enable(USART1, USART_INT_RBNE);

    usart1_send_string("usart1 ready!");
}



void USART1_SendArray(uint8_t *buf,uint8_t len)
{
    uint8_t t;
    for(t=0;t<len;t++)
    {
       usart_data_transmit(USART1,buf[t]);
       while (RESET == usart_flag_get(USART1, USART_FLAG_TBE));
    }
    while(usart_flag_get(USART1, USART_FLAG_TC) == RESET);

}

void usart1_send_string(char *send_pt)
{
     USART1_SendArray((uint8_t *)send_pt,strlen(send_pt));
}

