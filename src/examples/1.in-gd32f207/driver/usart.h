#ifndef __USART_H
#define __USART_H

#include "gd32f20x.h"

void usart1_init(void);
void usart1_send_string(char *send_pt);

#endif

