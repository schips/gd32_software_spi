#ifndef SPI_SF_H
#define SPI_SF_H

#define IOPORT_DIR_OUTPUT           1
#define IOPORT_DIR_INPUT            0

#define HIGH_OUT                1
#define LOW_OUT                 0

typedef void (*voidFun)(void);
typedef unsigned char (*getFun)(void);
typedef void (*setFun)(unsigned char);

// Define SPI communication mode
typedef enum
{
    Mode_1,   /* Clock Polarity is 0 and Clock Phase is 0 */
    Mode_2,   /* Clock Polarity is 0 and Clock Phase is 1 */
    Mode_3,   /* Clock Polarity is 1 and Clock Phase is 0 */
    Mode_4,   /* Clock Polarity is 1 and Clock Phase is 1 */
}SPIMode;


// Your are Main side, following object for your device control.
struct spi_device_info
{
    SPIMode cmm_mode;

    setFun  do_delay_us;
    //voidFun init;

    // CS
    setFun  cfg_nss_dir;
    voidFun set_nss_pin;
    voidFun clr_nss_pin;

    // CLK
    setFun  cfg_clk_dir;
    voidFun set_clk_pin;
    voidFun clr_clk_pin;

    // SDO/MOSI
    setFun  cfg_sdo_dir;
    voidFun set_sdo_pin;
    voidFun clr_sdo_pin;

    // SDI/MISO
    setFun cfg_sdi_dir;
    getFun get_sdi_pin;
};

// Function prototypes
void SPISimulateInit(struct spi_device_info *pSPIdev);

void SPISimulate_CSIsEnable(struct spi_device_info *pSPIdev, int IsEnable );

void SPISimulate_WriteByte(struct spi_device_info *pSPIdev, unsigned char nByte );

void SPISimulateWrite(struct spi_device_info *pSPIdev, unsigned char *pdata, int len );

unsigned char SPISimulate_ReadByte(struct spi_device_info *pSPIdev);

void SPISimulateRead(struct spi_device_info *pSPIdev, unsigned char *pdata, int len );

#endif /*SPI_CUSTOM_H*/

